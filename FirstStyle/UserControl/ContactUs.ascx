﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.ascx.cs" Inherits="FirstStyle.UserControl.ContactUs" %>
<div id="side">
    <h3>Contact Me</h3>
    <asp:Image id="profileimg" runat="server" ImageUrl="~/images/default.jpg"/>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label class="formlabel">Name:</label></div>
        <div class="col-md-10 col-xs-12">
            <asp:TextBox runat="server" ID="lead_name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="lead_name"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label class="formlabel">Email:</label></div>
        <div class="col-md-10 col-xs-12">
            <asp:TextBox runat="server" ID="lead_email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="lead_email"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label class="formlabel">Inquiry:</label></div>
        <div class="col-md-10 col-xs-12">
            <asp:DropDownList runat="server">
                <asp:ListItem>-Choose One-</asp:ListItem>
                <asp:ListItem>Interview</asp:ListItem>
                <asp:ListItem>Tutoring</asp:ListItem>
                <asp:ListItem>Contract Work</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList>

        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label class="formlabel">Message:</label></div>
        <div class="col-md-10 col-xs-12">
            <asp:TextBox runat="server" TextMode="multiline" Columns="50" Rows="5"  ID="lead_message"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="lead_message"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <asp:Button runat="server" OnClick="Submit_Form" Text="Submit"/>
    </div>
    <asp:Label runat="server" ID="lead_thankyou"></asp:Label>
</div>
