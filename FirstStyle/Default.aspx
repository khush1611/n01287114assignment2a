﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstStyle._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="LeftSideOne" runat="server">
    <div class="jumbotron">
        <div class="row">
         <div class="col-md-6">
            <h2>My College</h2>
            <p><!-- Image of college-->
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item item active">
                            <img class="d-block w-100 " src="images/humber1.jpg" alt="First slide">
                         </div>
                        
                         <div class="carousel-item item">
                            <img class="d-block w-100 " src="images/humber2.jpg" alt="Second slide">  
                         </div>
                    </div>
                        
                        
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                    </div>
                        </p>
                        <p>
                            <a class="btn btn-default" href="https://www.humber.ca/">Humber &raquo;</a>
                        </p>
        </div>
       <!--Humber image section ends here -->
            
            <div class="col-md-6">
                <h2>Address</h2>
                <address> 205 Humber College Blvd, Etobicoke, 
                 ON M9W 5L7</address>
            </div>
   
    </div>
    </div>
</asp:Content>

