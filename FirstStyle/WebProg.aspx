﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebProg.aspx.cs" Inherits="FirstStyle.WebProg" %>

<asp:Content ID="Content2" ContentPlaceHolderID="LeftSideOne" runat="server">
      <div class="jumbotron">
        <h4> HTTP-5103-0NB Web Programming</h4>
          JavaScript is the programming language of HTML and the Web.
          JavaScript is easy to learn.
          Learning the subject javascript is fun
       </div>
</asp:Content>

<asp:content ID="LeftIDTwo" ContentPlaceHolderID="LeftSideTwo" runat="server">
    <div class="jumbotron">
        <h4> HTTP-5103-0NB Web Programming</h4>
        I was not familiar with a javascript programming language.
        I found difficulties in javascript subject in for loop, while and do while loop.
    </div>
</asp:Content>

<asp:content ID="RightIDOne" ContentPlaceHolderID="RightBarOne" runat="server">
    <div class="jumbotron">
<h4> Sinppet of Code</h4>
<pre>
&lthtml&gt
   &lthead&gt
       &lttitle&gt JavaScript&lt/lttitle&gt
          &ltscript&gt
              var firstname = "khushboo";
               &ltalert("hey" +firstname)&gt
          &lt/ltscript&gt
   &lt/lthead&gt
   &ltbody&gt
       &ltdiv&gt
            my first JavaScript page
        &lt/ltdiv&gt
    &lt/body&gt
 &lt/html&gt
</pre>
    </div>
  
</asp:content>
<asp:Content ID="RightIDTwo" ContentPlaceHolderID="RightBarTwo" runat="server">
   <div class="jumbotron">
       <h4> Explaining the code</h4
       <ul>
         <li> We define a JavaScript code in three different way.</li>
         <li> Here the first alert displays the alert box on the page.</li>
         <li> The keyword var use to declare the variables in JavaScript.</li>
         <li> = operator is use to assign a value to variable</li>
         <li> JavaScript varibale name must not start with number and it should not contain space.</li>
         <li>Most preferable method for declaring is camelCase.</li>
           <li>JavaScript and Java are completely different languages, both in concept and design.</li>
     </ul> 
       <h5>Click on the below button link to learn more </h5>
       <div>
        <p>
         <a class="btn btn-default" href="https://www.w3schools.com/js/">Click Me &raquo;</a>
        </p>
    </div>
   </div>
</asp:content>
