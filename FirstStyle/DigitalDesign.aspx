﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DigitalDesign.aspx.cs" Inherits="FirstStyle.DigitalDesign" %>
<asp:Content ID="LeftIDOne" ContentPlaceHolderID="LeftSideOne" runat="server">
<div class="jumbotron">
   <h4> HTTP-5104-0NB Digital Design</h4>
    HTML stands for Hyper Text Markup Language.
    As name said Digital Design, This subject is about HTML, CSS.
    One of the most interesting subject of my course is digital design.
</div>

</asp:Content>
<asp:content ID="LeftIDTwo" ContentPlaceHolderID="LeftSideTwo" runat="server">
    <div class="jumbotron">
        <h4> HTTP-5104-0NB Digital Design</h4>
            HTML describes the structure of Web pages using markup
            I didn't find this subject more difficult as I've learned about HTML, CSS from online tutorials
           But My concepts are more evident now than ever before.
    </div>
   
</asp:Content>
<asp:content ID="RightIDOne" ContentPlaceHolderID="RightBarOne" runat="server">
    <div class="jumbotron">
        <h4> Sinppet of code</h4>
        <pre>
&lthtml&gt
    &lthead&gt
        &lttitle&gt MyFirstPage &lt/title&gt 
    &lt/head&gt
    &ltbody&gt
        &ltp&gt
            &ltp&gt My First HTML Page &lt/p&gt
       &lt/p&gt
    &lt/body&gt
&lt/html&gt
</pre>
 </div>
  
</asp:content>
<asp:Content ID="RightIDTwo" ContentPlaceHolderID="RightBarTwo" runat="server">
   <div class="jumbotron">
       <h4> Explaining the code</h4>
     <ul>
         <li> Here &lthtml&gt  is the root of html page.</li>
         <li> &lthead&gt tag Contains meta data.</li>
         <li> &lttitle&gt  tag  defines the title of the page.</li>
         <li>&ltbody&gt  tag is the tag where we define content.</li>
         <li>&ltp&gt defines the paragraph. </li>
          <li>&lt/&gt  defines the closing tag.</li>
         <li>You can add CSS code as Well.</li>
     </ul> 
       <h5>Click on the below button to learn more </h5>
        <p>
         <a class="btn btn-default" href="https://www.w3schools.com/html/default.asp">Click Me &raquo;</a>
        </p>
        
   </div>
</asp:content>