﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DataBaseDesign.aspx.cs" Inherits="FirstStyle.DataBaseDesign" %>

<asp:Content ID="Content2" ContentPlaceHolderID="LeftSideOne" runat="server">
    <div class="jumbotron"> 
    <h4> HTTP-5105-0NB Databse Design And development</h4>
        Database Design and developing is an quite interesting subject.
        It is a standard language for storing, manipulating and retrieving data in databases.
        Database is an important part of any websites.
    </div>
</asp:Content>
<asp:content ID="LeftIDTwo" ContentPlaceHolderID="LeftSideTwo" runat="server">
    <div class="jumbotron">
        <h4> HTTP-5105-0NB Databse Design And development</h4>
        There is nothing difficult in this subject once you start practicing.
        I found difficulties while learning Group by. After giving more hours practicing , I've become good in making queries using group by too. 
    </div>
   
</asp:Content>

<asp:content ID="RightIDOne" ContentPlaceHolderID="RightBarOne" runat="server">
    <div class="jumbotron">
    <h4>Syntax</h4>
       <pre>
SELECT column1, column2, ...
FROM table_name;
       </pre> 
        
     <h4>Sinppet of code</h4>
        <pre>
SELECT * FROM tbl_Humber;
         </pre>
    </div>
  
</asp:content>
<asp:Content ID="RightIDTwo" ContentPlaceHolderID="RightBarTwo" runat="server">
   <div class="jumbotron">
       <h4>Explaining the code</h4>
<ul>
    <li>The SELECT statement is used to select data from a database.</li>
    <li>The data returned is stored in a result table, called the result-set.</li>
    <li>Here, column1, column2,are the field names of the table you want to select data from.</li>
    <li> ( * ) will SELECT all the columns of the selected table.</li>
</ul>
       You can learn more about DataBase from W3schools.
   </div>
</asp:content>
